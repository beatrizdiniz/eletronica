# Fonte de tensão

Projeto de uma fonte de tensão realizada para a disciplina de Eletrônica para Computação do curso de Ciências de Computação do ICMC-USP, ministrada por Eduardo Valle Simões.

O projeto irá iniciar com a tensão da tomada 127v com corrente alternada e vai transformar essa tensão em um tesão apropriada para o funcionamento da fonte que é de 3V-12v com corrente continua.


# Circuito

O circuito feito no falstad está disponível em: 


# Componentes e seus valores

| Componente | Especificação | Valor(unidade) | Quantidade Usada|
| ------ | ------ | ------ | ------ |
| Diodo de Zener | 13v 1w | R$0,50 | 1 |
| Diodo Retifricador | 1n4007 | R$0,20 | 1 |
| Led | Difuso 5mm | R$0,50 | 1 |
| Resistor | 1.6kΩ | R$0,07 | 1 |
| Resistor | 4.4kΩ | R$0,07 | 1 |
| Resistor | 1kΩ | R$0,07 | 1 |
| Potenciômetro | 1w | R$7,00 | 1 |
| Transitor | 25V-1A 0,8W-65MHZ | R$0,07 | 1 |
| Placa Padrão Fibra | 5x10cm | R$9,35 | 1 |
| Capacitor | 470μF x 34V | R$2,89 | 1 |
| Transformador | 24v | R$22,99 | 1 |

# Justificativa dos Componentes

**Transformador:** Responsável por reduzir a tensão de 127v para a tensão desejada de 3V-12V. <br>

**Diodo de Zener:** Responsável por regular a tensão máxima, que nesse caso é 12v. Se a tensão for menor que 12V o diodo não irá não conduzir e portanto não irá inteferir no circuito.<br>

**Resistores:** Responsáveis por limitar a corrente e impedir que a corrente ultrapasse os valores sos limites dos demais componentes.<br>

**Potenciômetro:** Responsável por permitir o controle da tensão entre 3V-12V.<br>

**Transitor:** Responsável por permitir que a corrente passe de forma ajustável.<br>

# Cálculo dos Componentes

Fonte de $`127V \cdot \sqrt{2} = 179,6V`$ <br>
Após ocorrer a diminuição da tensão $`\to 18V`$ <br>
Depois do transformador $`\to18V \cdot \sqrt{2} = 25,46V`$<br>
Com dois diodos cai $`\2V`$ <br>
Após a ponte de diodo $`\to 25,46 - 2 = 23,46V`$<br>
$`\10% Ripple = 0,1`$<br>
Voltagem da fonte $`= 23,46 \cdot (1 - (\dfrac{0,1}{2})) = 22,287V`$

Cálculo da corrente que saí do Capacitor:<br>

$`iLed = \dfrac{VFonte - VLed}{R} = \dfrac{22,287 - 2}{1000} = 0,0202 = 20,2mA`$


$`iZener = \dfrac{VFonte - VZener}{680} = \dfrac{22,287 - 13}{680} = 0,0136 = 13,6mA`$


$`iCarga = 100mA (especificação)`$


$`i = iLed + iZener + iCarga = 20,2 + 13,6 + 100 = 133,8mA`$


$`Capacitor = \dfrac{5 \cdot 133,8 \cdot 10^{-3}}{22,28 \cdot 60} = 499,7μF`$


# Projeto do Esquemático do PCB no EAGLE
![Esquemático PCB](./Imagens/photo_2022-07-19_12-13-51.jpg)

# Projeto PCB no programa Proteus
![Esquemático PCB](./Imagens/photo_2022-07-19_12-13-47.jpg)

# Vídeo do Funcionamento da Fonte
![Video](./Imagens/video_2022-07-19_12-14-00.mp4)

# Membros: 
* Beatriz Aparecida Diniz - 11925430 <br>
* Eduardo Neves Gomes da Silva - 13822710 <br>
* João Vitor Alves de Medeiros - 13726762 <br>
* João Vitor Pereira Candido - 13751131 <br>
