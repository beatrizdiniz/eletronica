# Alarme com Arduino e Sensor Ultrassônico

## Software

[Código utilizado para o funcionamento do arduino](sensordist.ino)

## Circuito

Foram utilizadas as seguintes conexões:
* Pino 13 como pino de saída, que seria o sinal enviado pelo sensor;
* Pino 12 como pino de entrada, que seria o sinal recebido pelo sensor;
* Pino 11 como pino de saída, que seria a porta que enviará o sinal para o led;
* Pino 10 como pino de saída, que seria a porta que enviará o sinal para o buzzer.

<img alt = "esquema arduino" src="./Videos e Imagens/esquemaArduino.jpg" width="400">

## Imagens do Projeto

<img alt = "foto projeto" src="./Videos e Imagens/document_5107544644200170352_Moment5.jpg" width="400">

<img alt = "foto projeto" src="./Videos e Imagens/document_5107544644200170352_Moment4.jpg" width="400">

## Vídeo do Arduino

![Video](./Videos e Imagens/document_5107544644200170352.mp4)

![Video](./Videos e Imagens/document_5107544644200170353.mp4)

## Membros: 
* Beatriz Aparecida Diniz - 11925430 <br>
* Eduardo Neves Gomes da Silva - 13822710 <br>
* João Vitor Alves de Medeiros - 13726762 <br>
* João Vitor Pereira Candido - 13751131 <br>
