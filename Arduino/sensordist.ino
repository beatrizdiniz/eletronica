//Definindo o 'trigger' no pino 13, que seria o sinal enviado pelo sensor.
#define trigPin 13 
//Definindo o 'echo' no pino 12, que seria o sinal recebido pelo sensor.
#define echoPin 12 
//Definindo a porta 11 como a porta que enviará o sinal para o led.
#define led 11
//Definindo a porta 10 como a porta que enviará o sinal para o buzzer.
#define buzzer 10

void setup()

{ Serial.begin (9600);

//Definindo o modo como cada pino será utilizado, sendo o pino de saída(output) ou de entrada (input)
pinMode(trigPin, OUTPUT);

pinMode(echoPin, INPUT);

pinMode(led, OUTPUT);

pinMode(buzzer, OUTPUT);

}

void loop()

{ long duration, distance;
//Inicializando o estado do trigger e do echo, depois, envia e recebe o pulso.
digitalWrite(trigPin, LOW);

delayMicroseconds(2);

digitalWrite(trigPin, HIGH);

delayMicroseconds(10);

digitalWrite(trigPin, LOW);
//Calcula o tempo levado para encontrar um obstáculo a partir do que foi recebido pelo echo.
duration = pulseIn(echoPin, HIGH);
//Calcula a distância em que foi encontrado obstáculo.
distance = (duration/2) / 29.1;
//Se a distância for menor que um determinado valor (ajustável), o led acende e o buzzer apita repetidas vezes (tempo de delay também ajustável).
if (distance < 40)

{ digitalWrite(led,HIGH);
  tone(buzzer,1500);
  delay(500);
}

else {
digitalWrite(led,LOW);
noTone(buzzer);
delay(500);
}


Serial.print(distance);

Serial.println(" cm");

delay(500);

}
